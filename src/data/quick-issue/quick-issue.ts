export default {
  PRIORITY: [
    {
      id: 0,
      value: '不指定',
    },
    {
      id: 1,
      value: '不重要',
    },
    {
      id: 2,
      value: '次要',
    },
    {
      id: 3,
      value: '主要',
    },
    {
      id: 4,
      value: '严重',
    },
  ],
  ISSUE_STATE: [
    '待办的',
    '进行中',
    '已完成',
    '已拒绝',
    '已确认',
    '设计中',
    '开发中',
    '已验证',
    '修复中',
    '已取消',
    '已挂起',
    '新建',
    '已接纳',
  ],
  SOLT_TIME: ['提交时间', '更新时间'],
  PR_STATE: [
    {
      VALUE: 'open',
      TEXT: '开启的',
    },
    {
      VALUE: 'closed',
      TEXT: '关闭的',
    },
    {
      VALUE: 'merged',
      TEXT: '合入的',
    },
  ],
};
