// 涉及的公网路径配置文件
const DATA_LINK = 'https://datastat.opengauss.org/';
const OSCHINA_LINK = 'https://my.oschina.net/';
const CSDN_LINK = 'https://blog.csdn.net/';
const CTO_LINK = 'https://blog.51cto.com/';
const MODB_LINK = 'https://www.modb.pro/';
const INFOQ_LINK = 'https://www.infoq.cn/';
const BILIBILI_LINK = 'https://space.bilibili.com/';
const ZHIZHU_LINK = 'https://www.zhihu.com/';
const ENMOEDU_LINK = 'https://enmoedu.com/';
const GITEE_LINK = 'https://gitee.com/';
const GITEE_LINK1 = 'http://gitee.com/';
const GIYHUB_LINK = 'https://github.com/';
const STATUS_LINK = 'https://status.opengauss.org/';


export {
  DATA_LINK,  GITEE_LINK1,
  OSCHINA_LINK,
  CSDN_LINK,
  CTO_LINK,
  MODB_LINK,
  INFOQ_LINK,
  BILIBILI_LINK,
  ZHIZHU_LINK,
  ENMOEDU_LINK,
  GITEE_LINK,
  STATUS_LINK,
  GIYHUB_LINK,

};
